import setuptools
import os

from pyscripts import build_helper

NAME = 'default'

release = build_helper.Release(os.getenv('RELEASE', 'minor'))
version = build_helper.handle_version(NAME, release=release)


setuptools.setup(
    name='k810switch',
    version=version.dumps(),
    install_requires=[
        'hid',
    ],
    author='atomikin',
    author_email='atomikin@yandex.ru',
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    entry_points=dict(
        console_scripts=[
            'k810-fn-switch=k810switch.app:main',
        ]
    )
)
build_helper.handle_version(NAME, save=version)
