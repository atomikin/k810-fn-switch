import argparse
import dataclasses
import pprint
import os
import sys
import enum

import hid

PRODUCT = 'Logitech K810'
VENDOR_ID = 1133
PRODUCT_ID = 45849


class FnMode(str, enum.Enum):
    ON = 'on'
    OFF = 'off'

    def __str__(self) -> str:
        return str(self.value)


@dataclasses.dataclass()
class Info:
    product_string: str
    vid: int
    pid: int
    serial: str
    path: str

    @classmethod
    def load(cls, info: dict) -> 'Info':
        return cls(
            info['product_string'],
            info['vendor_id'],
            info['product_id'],
            info['serial_number'],
            info['path'],
        )

    def __hash__(self) -> int:
        return hash(dataclasses.astuple(self))


class FnKeysSwitch:
    FN_KEYS_OFF = b'\x10\xff\x06\x15\x00\x00\x00'
    FN_KEYS_ON = b'\x10\xff\x06\x15\x01\x00\x00'

    _device: hid.Device

    def __init__(self, info: Info):
        self._device = hid.Device(
            info.vid, info.pid, info.serial, info.path
        )

    def enable(self):
        self._device.write(self.FN_KEYS_ON)

    def disable(self):
        self._device.write(self.FN_KEYS_OFF)

    def set_mode(self, mode: FnMode):
        if mode is FnMode.ON:
            self.enable()
        else:
            self.disable()


def get_info():
    def check_dev(info):
        
        if not info['serial_number']:
            return False

        if (info['vendor_id'], info['product_id']) != (VENDOR_ID, PRODUCT_ID):
            return False

        return True

    info = filter(check_dev, hid.enumerate())
    info = map(Info.load, info)
    return list(set(info))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('action', choices=list(FnMode), type=FnMode)
    args = parser.parse_args()

    info = get_info()

    if not info:
        print(f'Device "{PRODUCT}" not found', file=sys.stderr)
        sys.exit(1)

    try:
        fn_switch = FnKeysSwitch(info[0])
    except Exception as err:
        print('Error:', str(err), file=sys.stderr)
        sys.exit(1)

    fn_switch.set_mode(args.action)
    print(info[0].product_string, 'FN-keys:', str(args.action))

if __name__ == '__main__':
    main()
